#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

class Book {
private:
  std::string title;
  std::string author;
  std::string ISBN;
  int quantity;
public:
  std::string getTitle() {
    return title;
  }
  std::string getAuthor() {   
    return author;  
  }
  std::string getISBN() {
    return ISBN;
  }
  int getQuantity() {
    return quantity;
  }

  void setTitle(std::string title_) {
    title=title_;
  }
  void setAuthor(std::string author_) {  
    author=author_;
  }
  void setISBN (std::string ISBN_) {
    ISBN=ISBN_;
  }
  void setQuantity(int quantity_) {
    quantity=quantity_;
  }
};

class Node {
public:
  Book book;
  Node* next;
};

char menu() {
  int choice;
  std::cout<<std::endl;
  std::cout << "     ***********************************       " << std::endl;
  std::cout << "     ***********************************       " << std::endl;
  std::cout << "     ***        LIBRARY MENU         ***       " << std::endl;
  std::cout << "     ***********************************       " << std::endl;
  std::cout << "     ***      1. Add a Book          ***       " << std::endl;
  std::cout << "     ***      2. Search a Book       ***       " << std::endl;
  std::cout << "     ***      3. Delete a Book       ***       " << std::endl;
  std::cout << "     ***      4. Exit                ***       " << std::endl;
  std::cout << "     ***********************************       " << std::endl;
  std::cout << "     ***********************************       " << std::endl;
  std::cout<<"Enter your choice (1 to 4): ";
  std::cin>>choice;  
  return choice;
}

void print(Book *book) {    //function to print book details  
  std::cout<<std::endl;
  std::cout<<"#######################"<<std::endl;
  std::cout<<"Book details: "<<std::endl;
  std::cout<<"Title: " << book->getTitle()<<std::endl;
  std::cout<<"ISBN: "<<book->getISBN()<<std::endl;
  std::cout<<"Quantity: "<<book->getQuantity()<<std::endl;
  std::cout<<"Author: "<<book->getAuthor()<<std::endl;
  std::cout<<"#######################"<<std::endl<<std::endl;
  std::cout<<std::endl;
}

Node *node_construct(Book data, Node *next)   // creating node and assigning the data
{
  Node *node = new Node();
  node->book.setTitle(data.getTitle());
  node->book.setQuantity(data.getQuantity());
  node->book.setISBN(data.getISBN());
  node->book.setAuthor(data.getAuthor());
  node->next=next;
  return node;
}

Node *push(Node *head, Book data)
{
  return node_construct(data, head);
}

Book *search(Node *head) {
  std::string title;
  std::cout<<"Enter title of the book to search: ";
  std::cin.ignore();
  getline(std::cin, title);
  if(head==NULL) {
    return NULL;
  }
  Node *current;
  for (current=head; current->next!=NULL; current=current->next) {
    if(current->book.getTitle().compare(title)==0) {
      return &(current->book);
    }
  }
  if(current->book.getTitle().compare(title)==0) {
    return &(current->book);
  }
  return NULL;
}

Node *add(Node *head) {
  Book book;
  std::string title;
  std::string ISBN;
  int quantity;
  std::string author;
  std::cout<<"Enter the book's title: ";
  std::cin.ignore();
  getline(std::cin, title);
  std::cout<<"Enter ISBN: ";
  getline(std::cin, ISBN);
  std::cout<<"Enter the Author's name: ";   
  getline(std::cin, author);
  std::cout<<"Enter the Quantity: ";
  std::cin>>quantity;
  book.setISBN(ISBN);
  book.setQuantity(quantity);
  book.setTitle(title);
  book.setAuthor(author);
  head=push(head, book);
  return head;
}

void delete_(Node *head) {
  std::string title;
  std::cout<<"Enter title of the book to delete: ";
  std::cin.ignore();
  getline(std::cin, title);
  if(head==NULL) {
    std::cout << "No book with this title exists!" << std::endl;
    return;
  }
    // When node to be deleted is head node
  if(head->book.getTitle().compare(title)==0)   // to check if the input is same as book title
  {
    if(head->next==NULL) 
    {
      std::cout<<"Error! The node cannot be empty "<<std::endl;
      return;
    }
    head->book.setTitle(head->next->book.getTitle());
    head->book.setISBN(head->next->book.getISBN());
    head->book.setQuantity(head->next->book.getQuantity());
    head->book.setAuthor(head->next->book.getAuthor());
    Node *temp=head->next;
    head->next=head->next->next;
    free(temp);
    std::cout<<"Book deleted successfully"<<std::endl;
    return;
  }

  Node *prev = head;
  while(prev->next!=NULL&&prev->next->book.getTitle().compare(title)!=0)
  prev=prev->next;
  if(prev->next==NULL)
  {
    std::cout<<"Book not found. Please try again"<< std::endl;
    return;
  }
  Node *temp=prev->next;
  prev->next=prev->next->next;
  free(temp);
  std::cout<<"Book deleted successfully"<<std::endl;
  return;
}

std::string get_list(Node *head) {     // file reading & insertion to data structure
  std::string str = "";
  if (head==NULL) {
    return "";
  }
  Node *current;
  for (current = head; current->next != NULL; current = current->next) {
    str=str+current->book.getTitle();
    str=str+"\t";
    str=str+ current->book.getAuthor();
    str=str+"\t";
    str=str+current->book.getISBN();
    str=str+"\t";
    str=str+std::to_string(current->book.getQuantity());
    str=str+"\n";
  }
  str=str+current->book.getTitle();
  str=str+"\t";
  str=str+current->book.getAuthor();
  str=str+"\t";
  str=str+current->book.getISBN();
  str=str+"\t";
  str=str+std::to_string(current->book.getQuantity());
  str=str+"\n";
  return str;
}

void save(std::string filename, Node *head) {
  std::ofstream file;
  file.open(filename, std::ios::app);
  if (file.is_open())
  {
    file << get_list(head);
    file.close();
  }
  else 
    std::cout << "Error opening file";
}

                // eg:: argc[0] = main, [1] = books; run the program -> ./main books (books=file name)
int main(int argv, char *argc[]) {
  Node* head = NULL;   
  std::ifstream file;
  if(argv==2) {
    file.open(argc[1], std::ios::in); //open a file to perform read operation using file object
    if (file.is_open()){   //checking whether the file is open
      std::string x;
      while(getline(file, x)){ //read data from file object and put it into string
        int j=0;
        std::string t="\t";    // to ignore tab space
        int i=x.find(t);
        Book book;
        book.setTitle(x.substr(j,i-j));
        j=i+t.size();
        i=x.find(t,j);
        book.setAuthor(x.substr(j,i-j));
        j=i+t.size();
        i=x.find(t, j);
        book.setISBN(x.substr(j,i-j));
        j=i+t.size();
        i=x.find(t, j);
        std::stringstream degree(x.substr(j,i-j));
        int x=0;
        degree>>x;
        book.setQuantity(x);
        head=push(head,book);
      }
      file.close(); //close the file object.
    }
  }
  while(true) {
    int choice;
    choice=menu();
    switch(choice) {
      case 1:
        head=add(head);
        std::cout<<"Book added successfully"<<std::endl;
        break;
      case 2: {
        Book *book = search(head);
        if (book==NULL) {
          std::cout<<"No book with this title exists!"<<std::endl;
        } 
        else {
          print(book);
        }
      }
      break;
      case 3:
        delete_(head);
        break;
      case 4:
        save(argc[1],head);
        exit(0);
        return 0;
      default:
        std::cout<<"Invalid entry. Please try again"<<std::endl;
      }
    }
}