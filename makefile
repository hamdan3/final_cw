CXX=g++
CXXFLAGS=-std=c++11 -Wall -Wextra

SRCS=$(wildcard *.cpp)
OBJS=$(patsubst %.cpp, %.o, $(SRCS))
TESTS=$(wildcard testing/*.cpp)
TEST_OBJS=$(patsubst %.cpp, %.o, $(TESTS))
EXE=program

run: $(EXE)
	./$(EXE) books
 
all: $(EXE)

$(EXE): $(OBJS)
	$(CXX) $(OBJS) -o $(EXE)

test: $(TEST_OBJS)
	$(CXX) $(filter-out main.o,$(OBJS)) $(TEST_OBJS) -o tests
	./tests

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

testing/%.o: testing/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	$(RM) $(EXE)
	$(RM) *.o

.PHONY: all run clean test
